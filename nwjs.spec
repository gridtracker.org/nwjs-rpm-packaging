Name:           {{{ git_name name=nwjs }}}
Summary:        NW.js is an app runtime based on Chromium and node.js.
Version:        0.68.1
Release:        1%{?dist}
ExclusiveArch:  x86_64
Source0:        {{{ git_dir_pack }}}
License:        MIT
URL:            https://github.com/nwjs/nw.js
Group:          Development/Libraries

Packager:       Matthew Chambers <nr0q@gridtracker.org>
BuildRequires:  make wget
Requires:       libatomic1

%description
NW.js is an app runtime based on Chromium and node.js. You can 
write native apps in HTML and JavaScript with NW.js. It also lets 
you call Node.js modules directly from the DOM and enables a new 
way of writing native applications with all Web technologies.

# Because I'm just repacking pre-built binaries
# debugging doesn't make sense here, disabling it.
%global debug_package %{nil}

%prep
%setup -q

%build
VERSION=%{version} DESTDIR=${RPM_BUILD_ROOT} make build

%install
VERSION=%{version} DESTDIR=${RPM_BUILD_ROOT} make install

%post
ln -sf /usr/lib64/nwjs/nw /usr/bin/nw

%clean
DESTDIR=${RPM_BUILD_ROOT} make clean

%files
%{_libdir}/%{name}/
%license %{_docdir}/%{name}/
%ghost %{_bindir}/nw

%changelog
* Fri Oct 07 2022 Matthew Chambers <nr0q@gridtracker.org> - 0.68.1-1
- Upstream update
* Sat Mar 05 2022 Matthew Chambers <nr0q@gridtracker.org> - 0.60.0-1
- Upstream update
* Wed Dec 15 2021 Matthew Chambers <nr0q@gridtracker.org> - 0.59.0-1
- Upstream update
* Wed Oct 06 2021 Matthew Chambers <nr0q@gridtracker.org> - 0.54.0-1
- Update to a new upstream
* Thu Sep 30 2021 Matthew Chambers <nr0q@gridtracker.org> - 0.47.0-1
- First RPM package of NWJS
