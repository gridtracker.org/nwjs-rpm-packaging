.DEFAULT_GOAL := help

VERSION ?=
DESTDIR ?= ./build
BIN ?= $(DESTDIR)/usr/bin
ARCH := x86_64
EXTARCH ?= x64
LIB ?= $(DESTDIR)/usr/lib64/nwjs


DOC ?= $(DESTDIR)/usr/share/doc/nwjs

.PHONY: help
help:
	@echo "Specify a target to build:"
	@echo "   -> make install"
	@echo "      install nwjs in DESTDIR (default: ./build)"
	@echo "   -> make clean"
	@echo "      remove built files from DESTDIR"

.PHONY: clean
clean:
	@echo "Cleaning $(DESTDIR)..."
	rm -rf $(DESTDIR)/*

.PHONY: build
build:
	wget --no-check-certificate https://dl.nwjs.io/v$(VERSION)/nwjs-v$(VERSION)-linux-x64.tar.gz
	tar -xf nwjs-v$(VERSION)-linux-x64.tar.gz
	rm nwjs-v$(VERSION)-linux-x64.tar.gz

.PHONY: install
install:
	@echo "Installing nwjs in $(DESTDIR)..."
	install -Dcm 644 LICENSE $(DOC)/LICENSE
	mkdir -p $(LIB)
	cp -r nwjs-v$(VERSION)-linux-x64/* $(LIB)/
	mkdir $(BIN)
	touch $(BIN)/nw
